package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	//test abc 333
	//test abc 444
	//test abc 555
	//test abc 666	
	//edit-001 in branchbranch-004
	//edit-002 in branchbranch-004
	//edit-003 in branchbranch-004
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
